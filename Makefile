MDFILES = $(filter-out README%,$(wildcard *.md))
HTMLFILES = $(MDFILES:.md=.html)
PDFFILES = $(MDFILES:.md=.pdf)

all: $(HTMLFILES) $(PDFFILES)

%.html: %.md
	pandoc -o $@ --standalone --toc --mathjax $< 

%.pdf: %.md
	pandoc -o $@ --standalone --toc $< 

.PHONY: showpdf
showpdf: $(PDFFILES)
	evince $(PDFFILES) &

