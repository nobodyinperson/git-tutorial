---
title: Git Tutorial
author: Yann Büchau
---

# What is `git`?

`git` is a **version control system**, or **VCS** in short.

## Version Control System 

A version control system assists you at
each arbitrary work on **plain text files** by keeping all changes you make
available for later reuse or recap. It also enables multiple people working on
the same project if a remote server is used for synchronisation. This also
introduces a nice automatic backup feature.

Advantages of version control:

- keep your whole history of changes available
- be able to revert to any previous point in history
- easy collaboration of multiple people on the same project
- easy backup

Disadvantages of version control:

- only **plain text files** can sensibly be version controlled. This renders
  all binary data formats not really useful for version control:
    - Office-files (MS Office's native formats)
    - images
    - compressed data

## `git` Terms

The location where a collection of `git`-version-controlled data resides is
called a **repository**. A repository is actually nothing else than a normal
folder, nothing intimidating about that. Any folder can be turned into a
`git`-repository. In a repository, you will find a hidden `.git`-subfolder.
This is the location where the real `git`-magic happens. You do not have to care
about this subfolder at all. Just don't delete it! :-) 

A repository may be residing on your local disk or a USB-drive, anywhere normal
folders may be as well. If a repository resides on a server, this server is
called a **remote**.

In a repository, as it is actually only a normal folder, there can be pretty
much anything. If it makes sense to version control all of it is another
question. Of course, `git` can be told to ignore certain elements.

If you make any changes in your repository and find this current state
acceptable, you can save it into your repository's history with a **commit**. A
commit is a set of *changes* to one or more files in the repository. You will
first **stage the files for commit** whose changes you want to commit by
putting them into the **staging area**. Then only the changes introduced in the
files in the staging area will be committed. 

As you work on your project, you will make more and more
commits.  For every commit you will have to type in a **commit message** that
briefly describes what has happened in this commit.

You can only commit a set of changes onto a **branch**. A branch is a
sequence of commits. Simple `git`-repositories only have one single branch which
is mostly named `master`. Here every new commit just goes onto the top of the
`master`-branch and there is a linear history. This is most likely what you've
been thinking version control is about. But as the project's complexity grows
and maybe even more people want to work on it, a purely linear history can get
frustrating very quickly:

- *Who is working on what exactly right now?*
- *Oh, my last week's work was completely bogus, I want to start off from that one
  commit one week before.* - If you revert to that commit, then how to treat all
  the other people's changes during that time?

These problems can be solved by using **branches**. Starting from a
specific point in history, you *branch off* the main **master**-branch and start
working on your task. You then commit only to your own branch. When you are done
with your task, you **merge** your own branch back into the main
**master**-branch. This way, the **master** branch is always in a clean state
and only contains good-quality content. Each person can have their own branch
for their tasks.

# `git` Setup

## Installation

`git` is probably already installed on your system. If not, on Debian/Ubuntu
systems, run:

```bash
sudo apt-get -y update
sudo apt-get -y install git
```

## Basic Configuration

```bash
# name that will be saved into your commits
git config --global user.name "Darth Vader"
# email that will be saved into your commits
git config --global user.email "darth.vader@empire.com"
# let git output be colorful
git config --global color.ui auto
# your favourite plain text editor
git config --global core.editor mousepad
```

# Using `git`

## Getting a `git` repository

To get a `git` repository, you may either turn an existing directory into one,
or **clone** one from a remote server.

To turn the current directory into a `git`-repository, run

```bash
git init
```

To clone a `git` repository from somewhere, run

```bash
git clone PATH_TO_REMOTE_REPOSITORY
```

This will download the repository into a directory with the repository's name.

For example, to clone the repository containing this tutorial, run

```bash
git clone git@gitlab.com:nobodyinperson/git-tutorial
```

Then change into the created directory `git-tutorial`:

```bash
cd git-tutorial
```

## Seeing the status

To see what's currently going on in a repository, run

```bash
git status
# On branch master
# Your branch is up-to-date with 'origin/master'.
# Changes not staged for commit:
#   (use "git add <file>..." to update what will be committed)
#   (use "git checkout -- <file>..." to discard changes in working directory)

#         modified:   git-tutorial.md

# no changes added to commit (use "git add" and/or "git commit -a")
```

I run this very frequently, just to get an overview over what is changed. In
this case `git` tells me that there is one file `git-tutorial.md` which was
modified but I didn't **stage** it for commit yet.

## Adding files to the staging area

Adding modified files to the staging area is as easy as

```bash
git add git-tutorial.md
```

Now if we see the status again

```bash
git status
# On branch master                                  
# Your branch is up-to-date with 'origin/master'.   
# Changes to be committed:                          
#   (use "git reset HEAD <file>..." to unstage)     
                                                  
#         modified:   git-tutorial.md               
```

If we committed now, the file `git-tutorial.md` would be included.

## Resetting the staging area

To reset the staging area completely, run

```bash
git reset
```

This does not change any files! It only resets what files you want to commit
next.

## Committing changes

To commit the changes introduced in the files being in the staging area, run

```bash
git commit
```

Your editor should open, showing you basically a `git status` output and a
blank line at the top. In this line you insert your short description of your
changes. If you feel like adding more than one line of text, *leave one line
blank* and then add as many lines as you want.

## Publishing your changes to the remote

If a remote is set up - which is the case if you cloned the repository from
somewhere - you may publish and upload your changes to the remote by 

```bash
git push
```

## Updating your local repository

It is a good idea to frequently update your repository to be up-to-date with the
remote. To update your current branch, run 

```bash
git pull
```

## Creating your own branch

To create your own branch to work on, run

```bash
git checkout -b my-own-branch
```

You are now on the branch my-own-branch and can work as normal.

If you want to publish this branch to the remote, on the first time you do this
you have to do a 

```bash
git push -u origin
```

Which uploads your current branch to the remote - which is mostly called
**origin** - and sets your current local branch to follow the remote branch.
From then on you can just do `git push` and `git pull` as normal.
